package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	    String testPass = "Fail";
	    assert (testPass.equals("Fail"));
	}
	@Test
	public void contextLoadsSuccess1() {
	    String testPass = "Fail";
	    assert (testPass.equals("Fail"));
	}
	@Test
	public void contextLoadsSuccess2() {
	    String testPass = "Fail";
	    assert (testPass.equals("Fail"));
	}
	@Test
	public void contextLoadsSuccess3() {
	    String testPass = "Fail";
	    assert (testPass.equals("Fail"));
	}
	@Test
	public void contextLoadsSuccess4() {
	    String testPass = "Fail";
	    assert (testPass.equals("Fail"));
	}
}
